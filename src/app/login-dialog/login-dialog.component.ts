import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { LocalStorageService } from '../services/local-storage.service';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-login-dialog',
  templateUrl: './login-dialog.component.html',
  styleUrls: ['./login-dialog.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class LoginDialogComponent implements OnInit {
  form: FormGroup;
  public loginInvalid: boolean;
  private formSubmitAttempt: boolean;
  private returnUrl: string;
  submitted;
  showPassword = false
  constructor(
    private fb: FormBuilder, private localstorage: LocalStorageService, public mdRef: MatDialogRef<LoginDialogComponent>) { }

  ngOnInit(): void {

    this.form = this.fb.group({
      username: ['', Validators.email],
      password: ['', Validators.required]
    });

  }

  onSubmit() {
    this.loginInvalid = false;
    this.formSubmitAttempt = false;
    this.submitted = true;
    if (this.form.valid) {
      try {
        const username = this.form.get('username').value;
        const password = this.form.get('password').value;

        console.log(username);
        console.log(password);
        const localUsers = this.localstorage.fetchLocal('localData');

        if (localUsers) {
          const indexOf = localUsers.findIndex(element => element.userid === this.form.get('username').value);

          console.log(indexOf);


          console.log(typeof(this.form.get('username').value));
          console.log(typeof(localUsers[indexOf].userid));



          if (indexOf != null) {
            const pass =  localUsers[indexOf].password;
            if (pass === this.form.get('password').value) {
              const privateUserData: any = {
                email: this.form.get('username').value,
                userName: localUsers[indexOf].username
              }

              this.localstorage.setLocal('token', privateUserData);
              this.mdRef.close(true);
            } else {
              this.form.controls.password.setErrors({ unauthorized: true });
              console.log('password does not match');
            }

          } else {
            console.log('no user detected');

          }
        }







        // await this.authService.login(username, password);
      } catch (err) {
        console.log(err);
        this.loginInvalid = true;
      }
    } else {
      this.formSubmitAttempt = true;
    }
  }

}
