import { Component } from '@angular/core';
import { LocalStorageService } from './services/local-storage.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor(private localStorage: LocalStorageService, private activateRoute: ActivatedRoute, private router: Router) {
    const usersDB = [
      {
        userid: 'abc@media.com',
        password: 'abc123',
        username: 'tom'
      },
      {
        userid: 'def@media.com',
        password: 'def123',
        username: 'dick'
      }
    ]
    this.localStorage.setLocal('localData', usersDB);


    try {
      if (this.localStorage.fetchLocal('token')) {

      } else {
        // this.snackBar_ref.open('Please Login', 'Disco party!', {
        //   panelClass: ['success-snackbar'],
        //   duration: 1000,
        //   verticalPosition: 'top'
        // });
        this.router.navigate(['/']);
      }
    } catch (error) {
      console.log(error);

    }

  }


}
