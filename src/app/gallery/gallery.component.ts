import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { FullViewDialogComponent } from './full-view-dialog/full-view-dialog.component';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.css']
})
export class GalleryComponent implements OnInit {

  constructor(private md: MatDialog) { }

  ngOnInit(): void {
  }

  openDialog(imageUrl: string) {
    const dialogRef = this.md.open(FullViewDialogComponent, {
      height: '465px',
      width: '500px',
      data: imageUrl,
      autoFocus: false
    });
  }

}
