import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FullViewDialogComponent } from './full-view-dialog.component';

describe('FullViewDialogComponent', () => {
  let component: FullViewDialogComponent;
  let fixture: ComponentFixture<FullViewDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FullViewDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FullViewDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
