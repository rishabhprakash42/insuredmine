import { Component, OnInit, Inject, ViewEncapsulation } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-full-view-dialog',
  templateUrl: './full-view-dialog.component.html',
  styleUrls: ['./full-view-dialog.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class FullViewDialogComponent implements OnInit {

  imageUrl;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any, public mdRef: MatDialogRef<FullViewDialogComponent>) {
    this.imageUrl = this.data;
    console.log(this.imageUrl);

  }

  ngOnInit(): void {
  }

}
