import { Component, OnInit, Input } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { LoginDialogComponent } from '../login-dialog/login-dialog.component';
import { LocalStorageService } from '../services/local-storage.service';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  privateUser = false;
  isLoggedIn = false;
  userName;

  constructor(private md: MatDialog, private localstorage: LocalStorageService, private router: Router) {
    const getLocal = this.localstorage.fetchLocal('token');
    try {
      if (getLocal) {
        this.userName = getLocal.userName;
        this.privateUser = true;
        this.isLoggedIn = true;
      }

    } catch (error) {
      this.privateUser = false;
      this.isLoggedIn = false;
      console.log('getting error in fetching');
    }
  }

  ngOnInit(): void {
  }

  openDialog() {
    const dialog = this.md.open(LoginDialogComponent);


    dialog.afterClosed().subscribe(result => {
      if (this.localstorage.fetchLocal('token') && result) {
        console.log('closed');
        this.privateUser = true;
        this.isLoggedIn = true;
      }
    });
  }

  logOut() {
    const getLocal = this.localstorage.fetchLocal('token')
    try {
      this.localstorage.destroyLocal('token');
      this.privateUser = false;
      this.isLoggedIn = false;
      this.router.navigate(['/']);
    } catch (error) {
      console.log('getting error in fetching');
    }
  }

}
