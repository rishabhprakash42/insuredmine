import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { LoginDialogComponent } from '../login-dialog/login-dialog.component';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private md: MatDialog) {
   const dialog = this.md.open(LoginDialogComponent);

   dialog.afterClosed().subscribe(result => {
    console.log('closed');
   });
   }

  ngOnInit(): void {
  }

}
