import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LocalStorageService {
  isPrivateUser = new Subject<boolean>();
  constructor() { }

  getData(): Observable<boolean> {
    return this.isPrivateUser.asObservable();
}

  // setting to local storage
  setLocal(key: string, value: any): any {
    try {
      localStorage.setItem(key, JSON.stringify(value));
    } catch (err) {
      console.log('error is setting in local', err);
    }
  }

  // getting from local storage
  fetchLocal(key: string): any {
    try {
      return JSON.parse(localStorage.getItem(key));
    } catch (err) {
      console.log('error is gettin from local', err);
    }
  }

  // removing from local storage
  destroyLocal(key: string): any {
    try {
      localStorage.removeItem(key);
    } catch (err) {
      console.log('error is removing from local', err);
    }
  }
}
