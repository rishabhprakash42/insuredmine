import { Injectable } from '@angular/core';
import { CanLoad, CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { LocalStorageService } from './local-storage.service';
import { MatSnackBar, MatSnackBarModule } from '@angular/material/snack-bar';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService implements CanActivate {

  constructor(private localStorage: LocalStorageService, private snackBar_ref: MatSnackBar, private router: Router) { }

  // canActivate() {
  //   try {
  //     if (this.localStorage.fetchLocal('token')) {
  //       return true;
  //     } else {
  //       // let snackBarRef = this.snackBar_ref.open('Message archived', 'Disco party!', {
  //       //   duration: 1000,
  //       // });


  //       this.router.navigate(['/']);
  //       return false;
  //     }
  //   } catch (err) {
  //     console.log(err);

  //   }
  // }

  canActivate(next: ActivatedRouteSnapshot
    , state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

      try {
        if (this.localStorage.fetchLocal('token')) {
          return true;
        } else {
          // this.snackBar_ref.open('Please Login', 'Disco party!', {
          //   panelClass: ['success-snackbar'],
          //   duration: 1000,
          //   verticalPosition: 'top'
          // });
          this.router.navigate(['/']);
          return false;
        }
      } catch (error) {
        console.log(error);

      }


  }
}

