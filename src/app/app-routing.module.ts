import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomePageComponent } from './home-page/home-page.component';
import { AboutComponent } from './about/about.component';
import { GalleryComponent } from './gallery/gallery.component';
import { LoginComponent } from './login/login.component';
import { ParentComponentComponent } from './parent-component/parent-component.component';
import { AuthService } from './services/auth.service';


const routes: Routes = [
  {
    path: '',
    component: ParentComponentComponent,
    children: [
      {
        path: '',
        component: HomePageComponent
      },
      {
        path: 'about',
        component: AboutComponent
      },
      {
        path: 'gallery',
        component: GalleryComponent,
        canActivate: [AuthService]
      },
      // {
      //   path: 'login',
      //   component: LoginComponent
      // },
      {
        path: '**',
        redirectTo: ''
      }
    ]
  }


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
