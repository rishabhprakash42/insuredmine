# InsuremineAssignment

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.0.6.

## requirements
run npm install to have all the dependencies in your local system (node version > 10)

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

for more info contact me on rishabh.prakash42@gmail.com
